#!/bin/python3

import os
import sys

import skrf

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

def rad2deg(rad):
    return 180 * (rad / np.pi)

def decode_vector_difference(data, fc, fs, f_switch, plot_steps=False, print_samples=False):

    """Process SDR samples to figure out amp/phase differences oscillated between
       fc = modulation frequency on top of baseband
       fs = sample rate
       Assume modulation square wave (switch) of 33.3% duty cycle.
       Return measurement of 'long' section with respect to 'short' section.
       (in cal_test 'long' is the DUT, 'short' is the loopback reference) """

    duty = 0.333

    # First, remove any DC offset
    data_hpf_cutoff = (fc*0.25)
    data_hpf = signal.firwin(251, [data_hpf_cutoff / (fs/2)], pass_zero=False)
    data_hp = np.convolve(data, data_hpf)

    if plot_steps:
        plt.plot(data_hp.real)
        plt.plot(data_hp.imag)
        plt.show()

    N = len(data_hp)
    t = np.linspace(0, N/fs, N)

    # FIR filter used to extract baseband after IQ demod
    iq_lowpass_cutoff = (fc*0.03)
    iq_lowpass = signal.firwin(251, [iq_lowpass_cutoff / (fs/2)], pass_zero=True)

    # Extract baseband, only use a chunk in the middle (avoid SDR frequency change artifacts)
    iq_baseband = np.convolve(data_hp * np.sin(2*np.pi*fc*t), iq_lowpass)
    iq_baseband = iq_baseband[4000:10000]

    correllation_target = iq_baseband.real + iq_baseband.imag
    correllation_target -= correllation_target.mean()
    square = signal.square(2*np.pi*f_switch*t[0:len(iq_baseband)], duty=1-duty)
    corr = np.abs(signal.correlate(correllation_target, square, mode='same'))

    start = np.argmax(corr) % int(fs / f_switch)
    square = np.concatenate((np.zeros(start), square))

    duty_long = 0.3
    duty_short = 0.1
    mask_long = (signal.square(2*np.pi*f_switch*t[0:len(iq_baseband)], duty=duty_long)+ 1)/2
    mask_short = (signal.square(2*np.pi*f_switch*t[0:len(iq_baseband)], duty=duty_short) + 1)/2
    mask_long = 1-np.concatenate((np.zeros(start + int((fs/f_switch)*(1.0-duty-duty_long)/2)), mask_long))[0:len(iq_baseband)]
    mask_short = 1-np.concatenate((np.zeros(start + int((1 - duty + (duty-duty_short)/2)*(fs/f_switch))), mask_short))[0:len(iq_baseband)]

    sample_short = np.ma.array(iq_baseband, mask=mask_short).mean()
    sample_long = np.ma.array(iq_baseband, mask=mask_long).mean()

    if plot_steps:
        plt.plot(iq_baseband.real)
        plt.plot(iq_baseband.imag)
        plt.plot(correllation_target)
        plt.plot(square)
        plt.plot(mask_short)
        plt.plot(mask_long)
        plt.axvline(start, color='r', label="correllation maximum")

    if plot_steps:
        plt.show()

    return sample_long / sample_short

DO_FULL_CAL = False

if DO_FULL_CAL:

    freqs = np.linspace(500e6, 1500e6, 41)
    freqs_ghz = freqs/1e9

    parent = 'cal_test_2port'
    subdirs = ['open', 'short', 'load', '100r']
    pref = "s22_"
    directories = [os.path.join(parent, pref+s) for s in subdirs]

    cals = []

    for d in directories:
        print(d)

        s_points = []

        for f in freqs:
            f_short = str(f/1e6).split('.')[0]
            fname = "rx_{}mhz.npy".format(f_short)
            data = np.load(os.path.join(d, fname))
            v = decode_vector_difference(data, fc=100e3, fs=1e6, f_switch=1e3)
            print("{} MHz - {} dB @ {} deg".format(f_short, 20*np.log10(abs(v)), rad2deg(np.angle(v))))
            s_points.append(v)

        name = d.split('/')[1]
        cals.append(skrf.Network(frequency=freqs_ghz, s=s_points, z0=50, name=name))
        #cals[-1].plot_s_smith()
        #plt.show()

    print(cals)

    ideal_cals = [
            skrf.Network(frequency=freqs_ghz, s=[1.0]*len(freqs), z0=50, name='open_ideal'),
            skrf.Network(frequency=freqs_ghz, s=[-1.0]*len(freqs), z0=50, name='short_ideal'),
            skrf.Network(frequency=freqs_ghz, s=[0.0]*len(freqs), z0=50, name='load_ideal')
            ]

    measured_cals = cals[0:3]

    cal = skrf.OnePort(measured=measured_cals, ideals=ideal_cals)

    r100_meas = cals[3]
    r100_cal = cal.apply_cal(r100_meas)

    r100_cal.plot_s_smith(draw_labels=True, label_axes=True)
    plt.show()

else:
    data = np.load(sys.argv[1])
    v = decode_vector_difference(data, fc=100e3, fs=1e6, f_switch=1e3, plot_steps=True, print_samples=True)
    print("{} dB @ {} deg".format(20*np.log10(abs(v)), rad2deg(np.angle(v))))
