#!/bin/python3

import SoapySDR
from SoapySDR import * #SOAPY_SDR_ constants
from scipy import signal
import numpy as np

import logging
import matplotlib.pyplot as plt

logging.getLogger().setLevel(logging.INFO)

def sdr_create_instance(sdr_name):
    logging.info("Looking for SoapySDR-compatible devices...")
    results = SoapySDR.Device.enumerate()
    for result in results: logging.info(f"Found: {result}")
    args = dict(name=sdr_name)
    logging.info(f"Attempting to open device matching {args}")
    sdr = SoapySDR.Device(args)
    return sdr

def sdr_print_channels(sdr, direction):
    for channel in range(sdr.getNumChannels(direction)):
        chs = f"[CH{channel}]"
        antennas = sdr.listAntennas(direction, channel)
        gains = sdr.listGains(direction, channel)
        freqs = [(f.minimum(), f.maximum()) \
                 for f in sdr.getFrequencyRange(direction, channel)]
        logging.info(f"{chs} antennas: {antennas}")
        logging.info(f"{chs} gains: {gains}")
        logging.info(f"{chs} freqs: {freqs}")



sdr = sdr_create_instance("LimeSDR Mini")

logging.info("RX Channels --")
sdr_print_channels(sdr, SOAPY_SDR_RX)
logging.info("TX Channels --")
sdr_print_channels(sdr, SOAPY_SDR_TX)

sample_rate = 1e6

# Configure the LimeSDR mini
sdr.setSampleRate(SOAPY_SDR_RX, 0, sample_rate)
sdr.setFrequency(SOAPY_SDR_RX, 0, 100.0e6)
sdr.setAntenna(SOAPY_SDR_RX, 0, 'LNAW')

sdr.setSampleRate(SOAPY_SDR_TX, 0, sample_rate)
sdr.setFrequency(SOAPY_SDR_TX, 0, 100.0e6)
sdr.setAntenna(SOAPY_SDR_TX, 0, 'BAND1')

#setup a stream (complex floats)
rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32)
txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32)

BUF_SZ = 8192*2

modulation_frequency = 100e3

#create a re-usable buffer for rx samples
all_rms = []

def generate_cf32_sine(num_samps, freq, scale_factor=0.30):
    t = np.linspace(0, num_samps/sample_rate, num_samps)
    return np.sin(2*np.pi*freq*t)  * scale_factor

def generate_cf32_sine_shifted(num_samps, freq, scale_factor=0.05):
    t = np.linspace(0, num_samps/sample_rate, num_samps)
    sine_1 = np.sin(2*np.pi*freq*t) * scale_factor

    # Same as above, phase shifted 45 degrees, 1/10th amplitude
    sine_2 = np.sin(2*np.pi*freq*t+np.pi/4) * scale_factor * 0.1

    # Select between above 2 sinewaves using 1/3 duty cycle pwm square wave
    square_freq = freq / 100
    square = signal.square(2*np.pi*square_freq*t, duty=0.33)
    sq1 = 0.5* (square + 1)
    sq2 = (1 - sq1)

    return sine_1 * sq1 + sine_2 * sq2


freqs = np.linspace(500e6, 1500e6, 41)

def design_cutoff(f):
    return f / (sample_rate/2)

#receive some samples
for freq in freqs:
    print(f"Frequency: {freq/1e6}MHz")
    sdr.setFrequency(SOAPY_SDR_RX, 0, freq)
    sdr.setFrequency(SOAPY_SDR_TX, 0, freq)

    sdr.setGain(SOAPY_SDR_TX, 0, 50)
    sdr.setGain(SOAPY_SDR_RX, 0, 40)

    sdr.activateStream(txStream)
    tx_sine = generate_cf32_sine(BUF_SZ, modulation_frequency).astype(np.complex64)
    #tx_sine = generate_cf32_sine_shifted(BUF_SZ, modulation_frequency)
    np.save("tx.npy", tx_sine)
    tx_t0 = int(sdr.getHardwareTime() + 0.1e9) #100ms
    tx_flags = SOAPY_SDR_HAS_TIME | SOAPY_SDR_END_BURST
    status = sdr.writeStream(txStream, [tx_sine], len(tx_sine), tx_flags, tx_t0)
    if status.ret != len(tx_sine):
        raise Exception('transmit failed %s'%str(status))


    rx_flags = SOAPY_SDR_HAS_TIME | SOAPY_SDR_END_BURST
    rx_t0 = int(tx_t0)
    sdr.activateStream(rxStream, rx_flags, rx_t0, BUF_SZ)

    rx_buffs = np.array([], np.complex64)

    while True:
        rx_buff = np.array([0]*1024, np.complex64)
        status = sdr.readStream(rxStream, [rx_buff], len(rx_buff))
        if status.ret > 0:
            rx_buffs = np.concatenate((rx_buffs, rx_buff[:status.ret]))
        else:
            break

    print("Got:", len(rx_buffs))

    np.save("rx_{}mhz.npy".format(str(freq/1e6).split('.')[0]), rx_buffs)
    #plt.plot(rx_buffs)

    sdr.deactivateStream(txStream) #stop streaming
    sdr.deactivateStream(rxStream) #stop streaming

sdr.closeStream(rxStream)

#plt.show()
